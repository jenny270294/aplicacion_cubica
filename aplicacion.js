window.onload = function(){
    var data;
    alert("Cualquier cuadro en rosa indica que no es correcto \n Alturas sólo entre 5 y 25 \n Diámetros positivos");

    document.getElementById("alturaglobal").value = "5";
    document.getElementById("alturaglobal").addEventListener("change",asignar_alturas);
    document.addEventListener("change",calculo);
    document.getElementById("guardar").addEventListener("click",guardar);
    document.addEventListener("click", boton_clic);
    asignar_alturas();
    calculo();

    /*Funcion para obtener la ubicacion latitud y longitud*/
    window.navigator.geolocation.getCurrentPosition((a)=>{console.log(a);})
    window.navigator.geolocation.getCurrentPosition((a)=>{
        console.log(a.coords.latitude);
        console.log(a.coords.longitude);
        document.getElementById("ubicacion").textContent = "Latitud: ".concat(a.coords.latitude).concat(", Longitud: ").concat(a.coords.longitude);
        
    })

    navigator.serviceWorker.register('aplicacion_sw.js', ).then(function(registration) {
        console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
        //Registration failed
        console.log('ServiceWorker registration failed: ', err);
    });
    navigator.serviceWorker.ready.then("reload");
    
    /*Funcion para poner el valor de la altura global en todas las alturas*/
    function asignar_alturas(){
        var altura = document.getElementById('alturaglobal').value;
        console.log(altura);
        document.getElementById('altdiez').value = altura;
        document.getElementById('altquince').value = altura;
        document.getElementById('altveinte').value = altura;
        document.getElementById('altveinte5').value = altura;
        document.getElementById('alttreinta').value = altura;
        document.getElementById('alttreinta5').value = altura;
        document.getElementById('altcuarenta').value = altura;
        document.getElementById('altcuarenta5').value = altura;
        document.getElementById('altcincuenta').value = altura;
        document.getElementById('altcincuenta5').value = altura;
        document.getElementById('alt60').value = altura;
        document.getElementById('alt65').value = altura;
        document.getElementById('altsetenta').value = altura;
        document.getElementById('altsetenta5').value = altura;
        document.getElementById('altochenta').value = altura;
        document.getElementById('altochenta5').value = altura;
        document.getElementById('altnoventa').value = altura;
        document.getElementById('altnoventa5').value = altura;
    }

    /*Funcion que lee el archivo con las constantes en funcion de el tipo de madera escogido*/
    function calculo(){
        var tipo = document.getElementById("tipo_madera").value;
        var tabla = document.getElementById("tabla_defecto").checked;
        if(tabla){
            tipo = "defecto";
        }
        var direccion = tipo.concat(".json");
        console.log(direccion);

        fetch(direccion)
        .then(function(response) {
            return response.json();
        })
        .then(function(myJson) {
            console.log(myJson);
            data = myJson;
            console.log(data);
            cubicar(myJson);
        });
    }
    

    /*Funcion que hace los calculos*/
    function cubicar(datos){
        
        /*Obtenemos la constante del archivo para cada diametro*/
        var diez = datos['alturas'][busca_altura(datos,"diez")]['diametros'][0]['10a14'];
        var quince = datos['alturas'][busca_altura(datos,"quince")]['diametros'][0]['15a19'];
        var veinte = datos['alturas'][busca_altura(datos,"veinte")]['diametros'][0]['20a24'];
        var veinte5 = datos['alturas'][busca_altura(datos,"veinte5")]['diametros'][0]['25a29'];
        var treinta = datos['alturas'][busca_altura(datos,"treinta")]['diametros'][0]['30a34'];
        var treinta5 = datos['alturas'][busca_altura(datos,"treinta5")]['diametros'][0]['35a39'];
        var cuarenta = datos['alturas'][busca_altura(datos,"cuarenta")]['diametros'][0]['40a44'];
        var cuarenta5 = datos['alturas'][busca_altura(datos,"cuarenta5")]['diametros'][0]['45a49'];
        var cincuenta = datos['alturas'][busca_altura(datos,"cincuenta")]['diametros'][0]['50a54'];
        var cincuenta5 = datos['alturas'][busca_altura(datos,"cincuenta5")]['diametros'][0]['55a59'];
        var sesenta = datos['alturas'][busca_altura(datos,"60")]['diametros'][0]['60a64'];
        var sesenta5 = datos['alturas'][busca_altura(datos,"65")]['diametros'][0]['65a69'];
        var setenta = datos['alturas'][busca_altura(datos,"setenta")]['diametros'][0]['70a74'];
        var setenta5 = datos['alturas'][busca_altura(datos,"setenta5")]['diametros'][0]['75a79'];
        var ochenta = datos['alturas'][busca_altura(datos,"ochenta")]['diametros'][0]['80a84'];
        var ochenta5 = datos['alturas'][busca_altura(datos,"ochenta5")]['diametros'][0]['85a89'];
        var noventa = datos['alturas'][busca_altura(datos,"noventa")]['diametros'][0]['90a94'];
        var noventa5 = datos['alturas'][busca_altura(datos,"noventa5")]['diametros'][0]['95a99'];


        /*Obtengo el valor de cada grupo de diametros*/
        var cont10 = document.getElementById('diez').value;
        var cont15 = document.getElementById('quince').value;
        var cont20 = document.getElementById('veinte').value;
        var cont25 = document.getElementById('veinte5').value;
        var cont30 = document.getElementById('treinta').value;
        var cont35 = document.getElementById('treinta5').value;
        var cont40 = document.getElementById('cuarenta').value;
        var cont45 = document.getElementById('cuarenta5').value;
        var cont50 = document.getElementById('cincuenta').value;
        var cont55 = document.getElementById('cincuenta5').value;
        var cont60 = document.getElementById('sesenta').value;
        var cont65 = document.getElementById('sesenta5').value;
        var cont70 = document.getElementById('setenta').value;
        var cont75 = document.getElementById('setenta5').value;
        var cont80 = document.getElementById('ochenta').value;
        var cont85 = document.getElementById('ochenta5').value;
        var cont90 = document.getElementById('noventa').value;
        var cont95 = document.getElementById('noventa5').value;
    
        var resultado = 0;
        var pies = 0;

        /*Se calculan los metros cubicados y el número de pies totales*/
        resultado = cont10*diez + cont15*quince + cont20*veinte + cont25*veinte5 + cont30*treinta + cont35*treinta5 + cont40*cuarenta +
        + cont45*cuarenta5 + cont50*cincuenta + cont55*cincuenta5 + cont60*sesenta + cont65*sesenta5 + cont70*setenta + cont75*setenta5 + 
        + cont80*ochenta + cont85*ochenta5 + cont90*noventa + cont95*noventa5;
        
        pies = cont10*1 + cont15*1 + cont20*1 + cont25*1 + cont30*1 + cont35*1 + cont40*1 + cont45*1 + cont50*1 +
        + cont55*1 + cont60*1 + cont65*1 + cont70*1 + cont75*1 + cont80*1 + cont85*1 + cont90*1 + cont95*1;
        
        /*Se sacan los calculos por los outputs*/
        document.getElementById('metros').value = resultado;
        document.getElementById('pies').value = pies;
    }


    /*Funcion que busca en la variable de los datos leidos del archivo, 
    la posicion de la altura deseada*/
    function busca_altura(datos,valor){
        var variable = "alt".concat(valor);
        var altura_buscada = document.getElementById(variable).value;
        for(var j = 0 ; j < 20; j++){
            var num = datos['alturas'][j]['altura']
            if(altura_buscada == num){
                return j;
            }
        }
    }


    /*Esta funcion prepara los datos para guardarlos en el archivo descargable*/
    function guardar(){
        
        var string;
        var propietario = document.getElementById("propietario").value;
        var parroquia = document.getElementById("parroquia").value;
        var ubicacion = document.getElementById("ubicacion").textContent;
        var tipo = document.getElementById("tipo_madera").value;

        var altura = document.getElementById("alturaglobal").value;
        var mas_alturas = document.getElementById("sel_altura").checked;
        var alturas_dist;
        var alturas;
        if(mas_alturas){
            alturas_dist = "si";
            alturas = guardar_alturas();
        }else{
            alturas_dist = "no";
            alturas ="";
        }
        var diametros = guardar_diametros();

        var metros = document.getElementById("metros").value;
        var pies = document.getElementById("pies").value;
        string = "Propietario: " + propietario.concat("\nParroquia: "+parroquia)
        .concat("\nUbicacion: "+ubicacion).concat("\nTipo Madera: "+tipo)
        .concat("\nAltura: "+altura).concat("\nVarias alturas: "+alturas_dist)
        .concat(alturas).concat(diametros)
        .concat("\nMetros: "+metros).concat("\nNumero de pies: "+pies);
        saveTextAsFile(string, propietario);
    }


    /*Funcion que lee los diametros y los almacena en una variable con el 
    formato para guardarlos en el archivo descargable*/
    function guardar_diametros(){
        var diametros_guardados = "\n";

        /*Obtengo el valor de cada grupo de diametros*/
        var cont10 = document.getElementById('diez').value;
        var cont15 = document.getElementById('quince').value;
        var cont20 = document.getElementById('veinte').value;
        var cont25 = document.getElementById('veinte5').value;
        var cont30 = document.getElementById('treinta').value;
        var cont35 = document.getElementById('treinta5').value;
        var cont40 = document.getElementById('cuarenta').value;
        var cont45 = document.getElementById('cuarenta5').value;
        var cont50 = document.getElementById('cincuenta').value;
        var cont55 = document.getElementById('cincuenta5').value;
        var cont60 = document.getElementById('sesenta').value;
        var cont65 = document.getElementById('sesenta5').value;
        var cont70 = document.getElementById('setenta').value;
        var cont75 = document.getElementById('setenta5').value;
        var cont80 = document.getElementById('ochenta').value;
        var cont85 = document.getElementById('ochenta5').value;
        var cont90 = document.getElementById('noventa').value;
        var cont95 = document.getElementById('noventa5').value;

        diametros_guardados = "Diametros \n".concat("10-14 = " + cont10 + "\n")
        .concat("15-19 = " + cont15 + "\n").concat("20-24 = " + cont20 + "\n")
        .concat("25-29 = " + cont25 + "\n").concat("30-34 = " + cont30 + "\n")
        .concat("35-39 = " + cont35 + "\n").concat("40-44 = " + cont40 + "\n")
        .concat("45-49 = " + cont45 + "\n").concat("50-54 = " + cont50 + "\n")
        .concat("55-59 = " + cont55 + "\n").concat("60-64 = " + cont60 + "\n")
        .concat("65-69 = " + cont65 + "\n").concat("70-74 = " + cont70 + "\n")
        .concat("75-79 = " + cont75 + "\n").concat("80-84 = " + cont80 + "\n")
        .concat("85-89 = " + cont85 + "\n").concat("90-94 = " + cont90 + "\n")
        .concat("95-99 = " + cont95 + "\n");
       
        return diametros_guardados;
    }


    /*Funcion que lee las alturas y las almacena en una variable con el 
    formato para guardarlos en el archivo descargable*/
    function guardar_alturas(){
        
        var alturas ="\n";
        var altura = document.getElementById("alturaglobal").value;
        var alt10 = document.getElementById('altdiez').value;
        var alt15 = document.getElementById('altquince').value;
        var alt20 = document.getElementById('altveinte').value;
        var alt25 = document.getElementById('altveinte5').value;
        var alt30 = document.getElementById('alttreinta').value;
        var alt35 = document.getElementById('alttreinta5').value;
        var alt40 = document.getElementById('altcuarenta').value;
        var alt45 = document.getElementById('altcuarenta5').value;
        var alt50 = document.getElementById('altcincuenta').value;
        var alt55 = document.getElementById('altcincuenta5').value;
        var alt60 = document.getElementById('alt60').value;
        var alt65 = document.getElementById('alt65').value;
        var alt70 = document.getElementById('altsetenta').value;
        var alt75 = document.getElementById('altsetenta5').value;
        var alt80 = document.getElementById('altochenta').value;
        var alt85 = document.getElementById('altochenta5').value;
        var alt90 = document.getElementById('altnoventa').value;
        var alt95 = document.getElementById('altnoventa5').value;

        var campos = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, alt10, alt15, alt20, alt25, alt30, alt35, alt40, alt45, alt50, alt55, alt60, alt65, alt70, alt75, alt80, alt85, alt90, alt95];
        var aux = "";

        for (j = 10; j < 28; j++){
            if(campos[j] != altura){
                //La altura es distinta de los campos
                var diametro = busca_diametro(campos, j);
                if(aux != ""){
                    if(campos[j] != campos[j+1]){
                        aux = aux+" - " + diametro + " = " + campos[j]+ "\n";
                        alturas = alturas + aux;
                        aux = "";
                    }else if(j+1 == 28){
                        aux = aux+" - " + diametro + " = " + campos[j]+ "\n";
                        alturas = alturas + aux;
                        aux = "";
                    }
                }else{
                    aux = "Altura de diametros " + diametro;
                }
            }else{
                var diametro = busca_diametro(campos, j);
                if(aux != ""){
                    if(campos[j] != campos[j+1]){
                        aux = aux+" - " + diametro + " = " + campos[j]+ "\n";
                        alturas = alturas + aux;
                        aux = "";
                    }else if(j+1 == 28){
                      aux = aux+" - " + diametro + " = " + campos[j]+ "\n";
                      alturas = alturas + aux;
                      aux = "";
                    }
                }else{
                    aux = "Altura de diametros " + diametro;
                }
            }
        }
        return (alturas);
    }

    /*Funcion que se utiliza para saber que diametro 
    esta almacenado en el array*/
    function busca_diametro(campos, num){
        var diam;
        switch(num){
            case 10: diam ="10"; break;
            case 11: diam ="15"; break;
            case 12: diam ="20"; break;
            case 13: diam ="25"; break;
            case 14: diam ="30"; break;
            case 15: diam ="35"; break;
            case 16: diam ="40"; break;
            case 17: diam ="45"; break;
            case 18: diam ="50"; break;
            case 19: diam ="55"; break;
            case 20: diam ="60"; break;
            case 21: diam ="65"; break;
            case 22: diam ="70"; break;
            case 23: diam ="75"; break;
            case 24: diam ="80"; break;
            case 25: diam ="85"; break;
            case 26: diam ="90"; break;
            case 27: diam ="95"; break;
            default: diam=""; break;
        }
        return (diam);
    }
    
    /*Funcion que genera el archivo para descargar le 
    introduce los datos y lo descarga*/
    function saveTextAsFile(datos, nombre){
        var textToWrite = datos;
        var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
        var fileNameToSaveAs = nombre.concat(".txt");
        var downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;
        downloadLink.innerHTML = "My Hidden Link";
        window.URL = window.URL || window.webkitURL;
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
        downloadLink.click();
    }

    /*Funcion que destruye el evento de descarga*/
    function destroyClickedElement(event){
        document.body.removeChild(event.target);
    }

    /*Funcion que controla los click en pantalla y si es en un boton de 
    mas o menos incremeta o decrementa */
    function boton_clic(eventoObj){
        var evento = eventoObj.srcElement;
        var id  = evento.id;
        if (id.indexOf("mas") != -1 || id.indexOf("menos") != -1){
            var campos = id.split("_");
            var operacion = campos[0];
            var elemento = campos[1];
   
            var contador = document.getElementById(elemento).value;
            if(operacion == "mas"){
                contador = parseInt(contador)+1;
                document.getElementById(elemento).value=contador;
            }else if (operacion == "menos"){
                contador = parseInt(contador)-1;
                document.getElementById(elemento).value=contador;
            }
        }
        
        if(id.indexOf("altura") != -1){
            asignar_alturas();
        }
    cubicar(data);
    }
};
