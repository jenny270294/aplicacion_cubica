const cacheName = "applicPWA-v1";
const filesToCache = ["./","index.html","aplicacion.js","aplicacion.css","manifest.json","arbol_peque.png","arbol_grande.png","defecto.json","pino.json", "eucalipto.json" ];

self.addEventListener("install", function(event) {
  // Perform install steps
  console.log("[Servicework] Install");
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log("[ServiceWorker] Caching app shell");


      return cache.addAll(filesToCache) ;
    })
  );
});

self.addEventListener("activate", function(event) {
  console.log("[Servicework] Activate");
  event.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log("[ServiceWorker] Removing old cache shell", key);
          return caches.delete(key);
        }
      }));
    })
  );
});

self.addEventListener("fetch", (event) => {
  console.log("[ServiceWorker] Fetch");
  event.respondWith(fromCache(event.request));
  event.waitUntil(
    update(event.request))
    .then(refresh);
});

function fromCache(request){
  return caches.open(cacheName).then(function(cache){
    return cache.match(request);
  });
}

function update(request){
    console.log("[ServiceWorker] Update");
  return caches.open(cacheName).then(function(cache){
    return fetch(request).then(function (response){
      return cache.put(request, response.clone()).then(function(){
        return response;
      });
    });
  });
}

function refresh(response){
    console.log("[ServiceWorker] Refresh");
  return self.clients.matchAll().then(function(clients){
    clients.forEach(function(client){
      var message = {
        type: 'refresh',
        url: response.url,

        eTag: response.headers.get('ETag')
      };
    client.postMessage(JSON.stringify(message));
    });
  });
}